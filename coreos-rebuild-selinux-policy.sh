#!/bin/bash

chroot /sysroot semodule -Bn
# restorecon does nothing when no policy is loaded
#chroot /sysroot restorecon -rvmF /etc/selinux

# FIXME targeted shouldn't be hard-coded
chroot /sysroot setfiles -F \
	/etc/selinux/targeted/contexts/files/file_contexts /etc/selinux

%define dracutlibdir  %{_prefix}/lib/dracut
%define dracutconfdir %{dracutlibdir}/dracut.conf.d
%define dracutmoddir  %{dracutlibdir}/modules.d/97coreos-selinux

Name:    coreos-selinux-tweak
Version: 0.1
Release: 1%{?dist}
Summary: A tweak to make SELinux updates work on CoreOS

License: MIT

Source0: module-setup.sh
Source1: coreos-rebuild-selinux-policy.sh
Source2: 90-coreos-selinux.conf

BuildRequires: coreutils

Requires: policycoreutils
Requires: dracut

%description
TODO

%install
install -d %{buildroot}%{dracutmoddir}
install -pm 0755 %{SOURCE0} %{buildroot}%{dracutmoddir}
install -pm 0755 %{SOURCE1} %{buildroot}%{dracutmoddir}
install -d %{buildroot}%{dracutconfdir}
install -pm 0644 %{SOURCE2} %{buildroot}%{dracutconfdir}

%files
%{dracutmoddir}/module-setup.sh
%{dracutmoddir}/coreos-rebuild-selinux-policy.sh
%{dracutconfdir}/90-coreos-selinux.conf

%changelog
